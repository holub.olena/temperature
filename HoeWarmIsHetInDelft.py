#!/usr/bin/env python3

import re

import dryscrape
import requests
from bs4 import BeautifulSoup
from termcolor import colored


def main():
    """
    Get current temperature in Delft from https://www.weerindelft.nl/
    :return temperature
    """
    url = 'https://www.weerindelft.nl/'
    data_html = requests.get(url=url)
    soup = BeautifulSoup(data_html.content, 'html.parser')
    divs = soup.find_all('div')
    frame = None
    for div in divs:
        if div.find('iframe', {'id': 'ifrm_3'}):
            frame = div.find('iframe', {'id': 'ifrm_3'})
            break
    if not frame:
        return

    dryscrape.start_xvfb()
    session = dryscrape.Session()
    session.visit(frame.attrs['src'])
    response = session.body()
    soup = BeautifulSoup(response, 'lxml')
    span = soup.find(id="ajaxtemp")
    temperature = re.sub(r'[^0-9-.]', '', span.text)

    if not temperature:
        return 'Something wrong...'

    return f'{temperature} degrees Celsius'


if __name__ == '__main__':
    print(colored('*-' * 30, 'yellow'))
    print(colored(main(), 'red'))
    print(colored('*-' * 30, 'yellow'))
