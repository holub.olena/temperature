- [Local Environment](#local-environment)
- [Deployment](#deployment)
- [Code Style](#code-style)
- [Built With](#built-with)


## Local Environment

The project developed on Python3.7. 

#### Local project can be ran using Docker. 
Install docker and execute next commands:
```bash
docker build . -t current_temperature
docker run current_temperature:latest
```
#### Another way with virtual environment.

1. Install the necessary packages:
```bash
apt update
apt install python3-dev qt5-default libqt5webkit5-dev build-essential python-lxml python-pip xvfb
```

2. Install python and create local virtual environment:
```bash
sudo apt install python3.7
sudo apt install python3-virtualenv

virtualenv -p python3.7 venv
source venv/bin/activate
```
3. Install necessary requirements:
```bash
pip install -r requirements.txt
```

4. Run python script:
```bash
python HoeWarmIsHetInDelft.py
```

## Deployment
For run script in Gitlab pipeline push to the master and retry pipeline.

## Code Style

Python code follows [PEP 8](https://www.python.org/dev/peps/pep-0008/) style guide convention.
During development was used [pycodestyle](http://pycodestyle.pycqa.org/en/latest/) module to check code style. 
The following command checks Python script for compliance PEP 8:
```bash
pycodestyle --max-line-length=119 --verbose --repeat HoeWarmIsHetInDelft.py

```

## Built With

Technology                                                                                    | Used for
----------------------------------------------------------------------------------------------| ---------------------------------------
[Python 3.7](https://docs.python.org/3.7/)                                                    | Main programming language
[Docker](https://docs.docker.com/compose/)                                                    | Deployment automation
[Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)                      | Pulling data out of https://www.weerindelft.nl/
[Gitlab CI/CD](https://docs.gitlab.com/)                                                      | Run a pipeline and execute script

