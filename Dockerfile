FROM python:3.7
ENV PYTHONUNBUFFERED 1

RUN apt update -y && apt install -y python3-dev qt5-default libqt5webkit5-dev build-essential python-lxml python-pip xvfb

WORKDIR /temperature
COPY . /temperature
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ENTRYPOINT ["python", "HoeWarmIsHetInDelft.py"]

